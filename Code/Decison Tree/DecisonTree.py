import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import copy
from collections import Counter
import sys

from decision_tree_classifier import *

global current_Point
global data_fold
global dataType

np.set_printoptions(threshold=np.nan, suppress=True)

def decisiontree(folds, file, maxDepth, minSize, numFeatures):

    global data_fold, current_Point, dataType

    data_fold = folds

    data = pd.read_csv(file, delimiter = '\t', header=None)

    dataType = detectDataTypes(data.loc[[0]])
    dataType = np.array(dataType)

#     Generate point objects
    dataPoints = []
    col_size = len(dataType)

    for row in range(0, len(data)):
        # record = np.array(data.loc[row])
        # numericalData = record[dataType]
        # categoricalData = record[1-dataType]
        numericalData = []
        categoricalData = []

        for col in range(0, col_size-1):
            if(dataType[col] == 0):
                numericalData.append(data[col][row])
            else:
                categoricalData.append(data[col][row])

        truth = (data.loc[[row]][col_size-1])[row]

        if(truth == 1):
            newPoint = Point(numericalData, categoricalData, 1)
            dataPoints.append(newPoint)
        else:
            newPoint = Point(numericalData, categoricalData, 0)
            dataPoints.append(newPoint)


    kf = KFold(data_fold)
    kf.get_n_splits(dataPoints)

    dataPoints = np.array(dataPoints)

    total_accuracy = 0
    total_precision = 0
    total_recall = 0
    total_fMeasure = 0

    fold = 0

    for train_index, test_index in kf.split(dataPoints):

        training_set = dataPoints[train_index]
        test_set = dataPoints[test_index]
        rootnode = constructDecisionTree(training_set, maxDepth, minSize, numFeatures, 0)

        # print("depth =", depth(rootnode))

        truePositive = 0
        falseNegative = 0

        falsePositive = 0
        trueNegative = 0

        for test_sample in test_set:
            test_sample.truth = predictclass(test_sample, rootnode)

            if test_sample.getTruth() == test_sample.getGroundTruth():
                if test_sample.getTruth() == 1:
                    truePositive += 1
                else:
                    trueNegative += 1
            else:
                if test_sample.getTruth() == 1:
                    falsePositive += 1
                else:
                    falseNegative += 1

        accuracy = (truePositive + trueNegative) / float(truePositive + falseNegative + falsePositive + trueNegative)
        precision = (truePositive)/float(truePositive + falsePositive)
        recall = (truePositive)/float(truePositive + falseNegative)
        F1_Measure = (truePositive + truePositive)/float(truePositive + truePositive + falseNegative + falsePositive)

        fold += 1
        print("Performance metrics for fold ", fold)
        print("accuracy=", accuracy)
        print("precision=", precision)
        print("recall=", recall)
        print("fMeasure=", F1_Measure)
        print("===========================")

        total_accuracy += accuracy
        total_precision += precision
        total_recall += recall
        total_fMeasure += F1_Measure

    print("Average accuracy= ", total_accuracy/folds)
    print("Average precision= ", total_precision/folds)
    print("Average recall= ", total_recall/folds)
    print("Average fMeasure= ", total_fMeasure/folds)


filePath = sys.argv[1]
decisiontree(10, filePath, maxDepth=9999, minSize=0, numFeatures = 1)
