Running Decision Tree:

Note: Make sure the necessary input files in the Input folder before running the program.

1. Invoke the program as follows - python3 DecsionTreeDriver.py <filename>

2. The program will output the Accuracy, Precision, Recall and F1-measure for each particular fold.

3. The average measures of Accuracy, Precision, Recall and F1-measure are printed towards the end.

Note: By default the number of folds are 10.


Example:
	python3 DecsionTreeDriver.py ../Input/project3_dataset1.txt
