import pandas as pd
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import scale
from scipy.stats import zscore
from sklearn.preprocessing import normalize
import numpy as np
import heapq
import collections
from collections import Counter
import sys

global current_Point
global data_fold
global kNeighbours
global dataType

class Point:
    def __init__(self, continousVals, categoricalVals, groundTruth):
        self.contVals = np.array(continousVals)
        self.catVals  = np.array(categoricalVals)
        self.truth = -1
        self.groundTruth = groundTruth

    def getContVals(self):
        return self.contVals

    def getCatVals(self):
        return self.catVals

    def getGroundTruth(self):
        return self.groundTruth

    def getTruth(self):
        return self.truth

def distance(point1, point2):
    global dataType

    categoricalDiffSum = 0
    continousDiffSum = 0

    for idx in range(len(point1)):
        if(dataType[idx] == 1 and point1[idx] != point2[idx]):
            # print(idx)
            categoricalDiffSum += 1
        else:
            continousDiffSum += np.square((point1[idx] - point2[idx]))

    dist = continousDiffSum + categoricalDiffSum
    return np.sqrt(dist)

# Generates the datatypes of the columns
def detectDataTypes(data):

    dataType = []

    for col in data.columns.values:
        if data[col].dtypes=='object':
            dataType.append(1)
        else:
            dataType.append(0)

    return dataType

def znorm(train_x, test_x):

    mean = np.mean(train_x, axis = 0)
    std = np.std(train_x, axis = 0)

    train_x = (train_x - mean)/std
    test_x = (test_x - mean)/std

    return train_x, test_x

def getContDistance(point1, point2):

    point1 = np.array(point1)
    point2 = np.array(point2)

    dist = point1 - point2
    dist = np.square(dist)
    dist = np.sum(dist)
    dist = np.sqrt(dist)

    return dist

def getCatDistance(point1, point2):

    dist = 0

    for i in range(len(point1)):
        if(point1[i] != point2[i]):
            dist += 1

    
    return dist

def predictClass(continuous_values_train, categorical_values_train, continuous_values_test, categorical_values_test, train_labels, kNeighbours):

    continuous_dist = []
    for i in range(len(continuous_values_train)):
        continuous_dist.append(getContDistance(continuous_values_train[i], continuous_values_test))

    categorical_dist = []

    for i in range(len(continuous_values_train)):
        categorical_dist.append(getCatDistance(categorical_values_train[i], categorical_values_test))

    continuous_dist = np.array(continuous_dist)
    categorical_dist = np.array(categorical_dist)

    distances = continuous_dist + categorical_dist

    
    truthFromNearestNeighbors = [x for _,x in sorted(zip(distances,train_labels))]
    truthFromNearestNeighbors = truthFromNearestNeighbors[:kNeighbours]

    count = Counter(truthFromNearestNeighbors)

    return count.most_common()[0][0]




def Knn(folds, neighbors, file):
    global data_fold, kNeighbours, current_Point, dataType

    total_accuracy = 0
    total_precision = 0
    total_recall = 0
    total_fMeasure = 0

    data_fold = folds
    kNeighbours = neighbors

    np.set_printoptions(threshold=np.nan, suppress=True)

    data = pd.read_csv(file, delimiter = '\t', header=None)

    # print(data.loc[[0]])
    dataType = detectDataTypes(data.loc[[0]])
    dataType = np.array(dataType)

    dataPoints = []
    col_size = len(dataType)
    for row in range(0, len(data)):
        numericalData = []
        categoricalData = []
        for col in range(0, col_size-1):
            if(dataType[col] == 0):
                numericalData.append(data[col][row])
            else:
                categoricalData.append(data[col][row])

        truth = (data.loc[[row]][col_size-1])[row]

        if(truth == 1):
            newPoint = Point(numericalData, categoricalData, 1)
            dataPoints.append(newPoint)
        else:
            newPoint = Point(numericalData, categoricalData, 0)
            dataPoints.append(newPoint)

    fold = 0

    kf = KFold(n_splits=folds)
    kf.get_n_splits(dataPoints)

    dataPoints = np.array(dataPoints)

    total_accuracy = 0
    total_precision = 0
    total_recall = 0
    total_fMeasure = 0

    for train_index, test_index in kf.split(dataPoints):

        training_set = dataPoints[train_index]
        test_set = dataPoints[test_index]

        continuous_values_train = []
        categorical_values_train = []
        train_labels = []
        for point in training_set:
            continuous_values_train.append(point.getContVals())
            categorical_values_train.append(point.getCatVals())
            train_labels.append(point.getGroundTruth())

        continuous_values_test = []
        categorical_values_test = []
        for point in test_set:
            continuous_values_test.append(point.getContVals())
            categorical_values_test.append(point.getCatVals())

        continuous_values_train, continuous_values_test = znorm(continuous_values_train, continuous_values_test)

        truePositive = 0
        falseNegative = 0

        falsePositive = 0
        trueNegative = 0
        for i in range(len(test_set)):
            test_set[i].truth = predictClass(continuous_values_train, categorical_values_train, continuous_values_test[i], categorical_values_test[i], train_labels, kNeighbours)

            if test_set[i].getTruth() == test_set[i].getGroundTruth():
                if test_set[i].getTruth() == 1:
                    truePositive += 1
                else:
                    trueNegative += 1

            else:
                if test_set[i].getTruth() == 1:
                    falsePositive += 1
                else:
                    falseNegative += 1

        accuracy = (truePositive + trueNegative) / float(trueNegative + truePositive + falseNegative + falsePositive)
        precision = (truePositive)/float(truePositive + falsePositive)
        recall = (truePositive)/float(truePositive + falseNegative)
        fMeasure = (truePositive + truePositive)/float(truePositive + truePositive + falseNegative + falsePositive)

        fold += 1
        print("Performance metrics for fold ", fold)
        print("accuracy=", accuracy)
        print("precision=", precision)
        print("recall=", recall)
        print("fMeasure=", fMeasure)
        print("===========================")

        total_accuracy += accuracy
        total_precision += precision
        total_recall += recall
        total_fMeasure += fMeasure
    
    print("Average accuracy= ", total_accuracy/folds)
    print("Average precision= ", total_precision/folds)
    print("Average recall= ", total_recall/folds)
    print("Average fMeasure= ", total_fMeasure/folds)


filePath = sys.argv[1]
K = int(sys.argv[2])
Knn(10, K, filePath)
