Running Adaboost:

Note: Make sure the necessary input files in the Input folder before running the program.

1. Invoke the program as follows - python3 Ada_boosting.py <filename>

2. The program will output the Accuracy, Precision, Recall and F1-measure for each particular fold.

3. The average measures of Accuracy, Precision, Recall and F1-measure are printed towards the end.

4. Other parameters such as number of trees and features to be considered can be modified in the program by changing the following line,

ada_boost(folds = 10, file = filePath, maxDepth = 999, minSize = 0, numTrees = 5, numFeatures = 1, numRecords = 1)

numTrees = 10   (by default, can be increased to experiment) - this represents the number of models created in each round.

Note: By default the number of folds are 10.

Example:
	python3 Ada_boosting.py ../Input/project3_dataset1.txt
