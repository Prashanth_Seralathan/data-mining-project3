import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import copy
from collections import Counter
from random import randrange
import sys

from decision_tree_classifier import *

global current_Point
global data_fold
global dataType


def ada_boost(folds, file, maxDepth, minSize, numTrees, numFeatures, numRecords):
	global data_fold, current_Point, dataType
	data_fold = folds

	data = pd.read_csv(file, delimiter = '\t', header=None)

	dataType = detectDataTypes(data.loc[[0]])
	dataType = np.array(dataType)

#     Generate point objects
	dataPoints = []
	col_size = len(dataType)


	for row in range(0, len(data)):
		# record = np.array(data.loc[row])
		# numericalData = record[dataType]
		# categoricalData = record[1-dataType]

		numericalData = []
		categoricalData = []


		for col in range(0, col_size-1):

			if(dataType[col] == 0):
				numericalData.append(data[col][row])
			else:
				categoricalData.append(data[col][row])


		truth = (data.loc[[row]][col_size-1])[row]
		if(truth == 1):
			newPoint = Point(numericalData, categoricalData, 1)
			dataPoints.append(newPoint)
		else:
			newPoint = Point(numericalData, categoricalData, 0)
			dataPoints.append(newPoint)

	kf = KFold(data_fold)
	kf.get_n_splits(dataPoints)

	dataPoints = np.array(dataPoints)

	total_accuracy = 0
	total_precision = 0
	total_recall = 0
	total_fMeasure = 0

	fold = 0

	for train_index, test_index in kf.split(dataPoints):
		training_set = dataPoints[train_index]
		test_set = dataPoints[test_index]

		roots = []
		weights = []

		record_weights = [1.0/len(training_set)]*len(training_set)

		for i in range(numTrees):
			sample_set = []
			for i in range(int(len(training_set)*numRecords)):
				random_point = np.random.choice(training_set, p=record_weights)
				sample_set.append(random_point)

			record_weights = np.array(record_weights)

			root = constructDecisionTree(sample_set, maxDepth, minSize, numFeatures, 0)

			true_classes = []
			predicted_classes = []
			for record in training_set:
				true_classes.append(record.getGroundTruth())
				predicted_classes.append(predictclass(record, root))

			misclassifications = []
			for i in range(len(true_classes)):
				if true_classes[i] == predicted_classes[i]:
					misclassifications.append(0)
				else:
					misclassifications.append(1)

			misclassifications = np.array(misclassifications)

			error = np.sum(np.multiply(misclassifications, record_weights))/np.sum(record_weights)
			
			if(error>0.5):
				record_weights = [1.0/len(training_set)]*len(training_set)
				continue

			alpha = np.log((1-error)/error)*0.5

			np.putmask(misclassifications, misclassifications==0, -1)
			record_weights = np.multiply(record_weights, np.exp(alpha*misclassifications))

			record_weights = record_weights/(np.sum(record_weights))

			roots.append(root)
			weights.append(alpha)

		truePositive = 0
		falseNegative = 0

		falsePositive = 0
		trueNegative = 0


		for test_sample in test_set:

			classOneWeight = 0
			classZeroWeight = 0
			for i in range(len(roots)):
				truthLabel = predictclass(test_sample, roots[i])
				if truthLabel==1:
					classOneWeight+=weights[i]
				else:
					classZeroWeight+=weights[i]

			test_sample.truth = 0
			if classOneWeight > classZeroWeight:
				test_sample.truth = 1

			if test_sample.getTruth() == test_sample.getGroundTruth():
				if test_sample.getTruth() == 1:
					truePositive += 1
				else:
					trueNegative += 1
			else:
				if test_sample.getTruth() == 1:
					falsePositive += 1
				else:
					falseNegative += 1

		accuracy = (truePositive + trueNegative) / float(truePositive + falseNegative + falsePositive + trueNegative)
		precision = (truePositive)/float(truePositive + falsePositive)
		recall = (truePositive)/float(truePositive + falseNegative)
		fMeasure = (truePositive + truePositive)/float(truePositive + truePositive + falseNegative + falsePositive)

		fold += 1
		print("Performance metrics for fold ", fold)
		print("accuracy=", accuracy)
		print("precision=", precision)
		print("recall=", recall)
		print("fMeasure=", fMeasure)
		print("===========================")

		total_accuracy += accuracy
		total_precision += precision
		total_recall += recall
		total_fMeasure += fMeasure

	print("Average accuracy= ", total_accuracy/folds)
	print("Average precision= ", total_precision/folds)
	print("Average recall= ", total_recall/folds)
	print("Average fMeasure= ", total_fMeasure/folds)


filePath = sys.argv[1]
ada_boost(folds = 10, file = filePath, maxDepth = 999, minSize = 0, numTrees = 5, numFeatures = 1, numRecords = 1)


