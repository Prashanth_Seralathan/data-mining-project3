Running Random Forest:

Note: Make sure the necessary input files in the Input folder before running the program.

1. Invoke the program as follows - python3 Random_forest.py <filename>

2. The program will output the Accuracy, Precision, Recall and F1-measure for each particular fold.

3. The average measures of Accuracy, Precision, Recall and F1-measure are printed towards the end.

4. Other parameters such as number of trees and features to be considered can be modified in the program by changing the following line,

random_forest(10, filePath, maxDepth=1000, minSize=0, numTrees=10, numFeatures = 0.3)
numTrees = 10(by default, can be increased to experiment)
numFeatures = 0.3 - Takes 30% of the attributes randomly for each node.

Note: By default the number of folds are 10.

Example:
	python3 Random_forest.py Input/project3_dataset1.txt
