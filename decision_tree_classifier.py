import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import copy
from collections import Counter
import math
import random

class Point:
    def __init__(self, continousVals, categoricalVals, groudTruth):
        self.contVals = np.array(continousVals)
        self.catVals  = np.array(categoricalVals)
        self.truth = -1
        self.groundTruth = groudTruth

    def getContVals(self):
        return self.contVals

    def getCatVals(self):
        return self.catVals

    def getGroundTruth(self):
        return self.groundTruth

    def getTruth(self):
        return self.truth

class Node:

    def __init__(self, value, col, contorCat):
        self.label = -1
        self.isCont = not contorCat
        self.value = value
        self.isLeaf = False
        self.col = col
        self.left = None
        self.right = None

    def getLeft(self):
        return self.left

    def getRight(self):
        return self.right




def detectDataTypes(data):

    dataType = []

    for col in data.columns.values:
        if data[col].dtypes=='object':
            dataType.append(1)
        else:
            dataType.append(0)

    return dataType


def getFrequentClass(training_set):

    truthlabellist = []

    for i in range(0, len(training_set)):
        truthlabellist.append(training_set[i].getGroundTruth())

    count = Counter(truthlabellist)
    return count.most_common()[0][0]


def get_gini(list):

    truthvalues = set(list)
    count = 0

    for values in truthvalues:
        count += (list.count(values))**2

    gini = (1 - ((float(count) / len(list)**2)))
    # print(gini)
    return gini

def getginicont(leftSetOnes, leftsetZeroes, rightSetOnes, rightSetZeroes):

    leftCount = leftSetOnes + leftsetZeroes
    rightCount = rightSetOnes + rightSetZeroes

    if(leftCount == 0 or rightCount == 0):
        return 100


    leftGini = 1 - (leftSetOnes**2 + leftsetZeroes**2) / float(leftCount**2)
    rightGini = 1 - (rightSetOnes**2 + rightSetZeroes**2) / float(rightCount**2)

    return (leftGini)*(leftCount/float(leftCount + rightCount)) + (rightGini)*(rightCount)/float(leftCount + rightCount)


def getginicatg(training_set, col, currentVal, isCont):

    true_list = []
    false_list = []


    for row in range(0, len(training_set)):
        currentRecord = training_set[row]
        valarr = currentRecord.getCatVals()
        if(valarr[col] == currentVal):
            true_list.append(currentRecord.getGroundTruth())
        else:
            false_list.append(currentRecord.getGroundTruth())


    if(len(true_list) == 0 or len(false_list) == 0):
        return 100

    total_len = float(len(true_list) + len(false_list))
    gini = get_gini(true_list)*(len(true_list)/total_len) + get_gini(false_list)*(len(false_list)/total_len)

    # print(gini)
    return gini


def getbestsplit(training_set, numFeatues):

    bestgini = 1.0
    bestval = -1
    bestcol = -1
    isCatSplit = False

    currentrecord = training_set[0]
    colSize = len(currentrecord.getContVals())
    catColSize = len(currentrecord.getCatVals())

    totalSize = colSize + catColSize

    numberOfSamples = int(math.ceil(totalSize*numFeatues))

    randomIndexes = random.sample(range(0, totalSize), numberOfSamples)

    contIndexes = []
    catIndexes = []
    
    for index in randomIndexes:
        if index < colSize:
            contIndexes.append(index)
        else:
            catIndexes.append(index-colSize)

    #  Continous Values
    for col in range(0, colSize):
        if col not in contIndexes:
            continue
        prevElement = None
        # sort the training set based on attribute at col index
        training_set_clone = copy.deepcopy(training_set)
        training_set_clone = sorted(training_set_clone, key = lambda point : point.getContVals()[col])

        classes_one = 0
        totalClassesLen = len(training_set_clone)
        # Count classes
        for row in range(0, len(training_set_clone)):
            if(training_set_clone[row].getGroundTruth() == 1):
                classes_one += 1

        leftSetOnes = 0
        rightSetOnes = classes_one


        for row in range(0, len(training_set_clone)):
            currentrecord = training_set_clone[row].getContVals()
            currentVal = currentrecord[col]

            if(row != 0 and currentVal == prevElement):
                if(training_set_clone[row].getGroundTruth() == 1):
                    leftSetOnes += 1
                    rightSetOnes -= 1
                continue

            leftsetZeroes = row - leftSetOnes
            rightSetZeroes = totalClassesLen - row - rightSetOnes

            ginicurrent = getginicont(leftSetOnes, leftsetZeroes, rightSetOnes, rightSetZeroes)

            if(training_set_clone[row].getGroundTruth() == 1):
                leftSetOnes += 1
                rightSetOnes -= 1

            prevElement = currentVal
            # print ginicurrent

            if(ginicurrent < bestgini):
                bestgini = ginicurrent
                bestval = currentVal
                bestcol = col
                isCatSplit = False


    #  Categorical Values

    for col in range(0, catColSize):
        if col not in catIndexes:
            continue

        visitedSet = set()
        for row in range(0, len(training_set)):
            currentrecord = training_set[row].getCatVals()
            currentVal = currentrecord[col]
            if(currentVal in visitedSet):
                continue
            visitedSet.add(currentVal)
            ginicurrent = getginicatg(training_set, col, currentVal, False)
            # print ginicurrent

            if(ginicurrent < bestgini):
                bestgini = ginicurrent
                bestval = currentVal
                bestcol = col
                isCatSplit = True

    # print "return value = ",bestgini, bestval, bestcol, isCatSplit
    return bestgini, bestval, bestcol, isCatSplit


def partition(training_set, value, col, isCatSplit):
    leftset = []
    rightset = []

    # print value, col, isCatSplit
    for row in range(0, len(training_set)):
        currentrecord = training_set[row]
        if(isCatSplit):
            currentvals = currentrecord.getCatVals()
            if(value == currentvals[col]):
                leftset.append(currentrecord)
            else:
                rightset.append(currentrecord)
        else:
            currentvals = currentrecord.getContVals()
            if(currentvals[col] >= value):
                leftset.append(currentrecord)
            else:
                rightset.append(currentrecord)

    # print("left: ", leftset)
    # print("right: ", rightset)
    # print("----------")
    return leftset, rightset


def constructDecisionTree(training_set, maxDepth, minSize, numFeatues, depth = 0):

    giniNode=get_gini([point.getGroundTruth() for point in training_set])
    gini, value, col , isCatSplit = getbestsplit(training_set, numFeatues)
    # print "outside = ",gini, value, col, isCatSplit

    root = Node(value, col, isCatSplit)

    if(gini == 1 or giniNode==0):
        root.isLeaf = True
        root.value = getFrequentClass(training_set)
        return root

    leftset, rightset = partition(training_set, value, col, isCatSplit)

    if (len(leftset)==0 or len(rightset)==0):
        root.isLeaf = True
        root.value = getFrequentClass(training_set)
        return root

    # print("contructing subtrees ", depth)
    root.left = constructDecisionTree(leftset,maxDepth,minSize, numFeatues, depth+1)
    root.right = constructDecisionTree(rightset,maxDepth,minSize, numFeatues, depth+1)

    return root


def predictclass(test_sample, rootnode):
    if rootnode.isLeaf:
        return rootnode.value

    if(rootnode.isCont):
        current_value = test_sample.getContVals()[rootnode.col]
        if(current_value >= rootnode.value):
            return predictclass(test_sample, rootnode.left)
        else:
            return predictclass(test_sample, rootnode.right)
    else:
        # print(test_sample.getCatVals())
        # print(rootnode.col)
        current_value = test_sample.getCatVals()[rootnode.col]
        if(current_value == rootnode.value):
            return predictclass(test_sample, rootnode.left)
        else:
            return predictclass(test_sample, rootnode.right)

def depth(root):
    if root.isLeaf:
        return 1

    # print("===================")
    # print(root.isCont)
    # print(root.col)
    # print(root.value)
    # print("===================")

    return 1+max(depth(root.left), depth(root.right))