import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import copy
from collections import Counter
from random import randrange
import sys

from Decision_tree import *

global current_Point
global data_fold
global dataType

np.set_printoptions(threshold=np.nan, suppress=True)

def random_forest(folds, file, maxDepth, minSize, numTrees, numFeatures):
	global data_fold, current_Point, dataType
	data_fold = folds

	data = pd.read_csv(file, delimiter = '\t', header=None)

	dataType = detectDataTypes(data.loc[[0]])
	dataType = np.array(dataType)

#     Generate point objects
	dataPoints = []
	col_size = len(dataType)


	for row in range(0, len(data)):
		# record = np.array(data.loc[row])
		# numericalData = record[dataType]
		# categoricalData = record[1-dataType]

		numericalData = []
		categoricalData = []


		for col in range(0, col_size-1):

			if(dataType[col] == 0):
				numericalData.append(data[col][row])
			else:
				categoricalData.append(data[col][row])


		truth = (data.loc[[row]][col_size-1])[row]
		if(truth == 1):
			newPoint = Point(numericalData, categoricalData, 1)
			dataPoints.append(newPoint)
		else:
			newPoint = Point(numericalData, categoricalData, 0)
			dataPoints.append(newPoint)


	kf = KFold(data_fold)
	kf.get_n_splits(dataPoints)

	dataPoints = np.array(dataPoints)

	total_accuracy = 0
	total_precision = 0
	total_recall = 0
	total_fMeasure = 0

	fold = 0

	for train_index, test_index in kf.split(dataPoints):

		training_set = dataPoints[train_index]
		test_set = dataPoints[test_index]
		
		roots = []
		for i in range(numTrees):
			# Sample training set
			sample_set = []

			random_indexes = random.sample(range(len(training_set)), int(len(training_set)*0.6))
			sample_set = training_set[np.array(random_indexes)]

			for i in range(int(len(training_set)*0.4)):
				random_index = randrange(0, len(sample_set))
				np.append(sample_set, [sample_set[random_index]])

			# print(random_indexes)

			roots.append(constructDecisionTree(sample_set, maxDepth, minSize, numFeatures, 0))

		truePositive = 0
		falseNegative = 0

		falsePositive = 0
		trueNegative = 0

		for test_sample in test_set:

			count = 0
			for root in roots:
				truthLabel = predictclass(test_sample, root)
				if truthLabel==1:
					count+=1

			test_sample.truth = 0
			if count >= len(roots)-count:
				test_sample.truth = 1

			if test_sample.getTruth() == test_sample.getGroundTruth():
				if test_sample.getTruth() == 1:
					truePositive += 1
				else:
					trueNegative += 1
			else:
				if test_sample.getTruth() == 1:
					falsePositive += 1
				else:
					falseNegative += 1

		accuracy = (truePositive + trueNegative) / float(truePositive + falseNegative + falsePositive + trueNegative)
		precision = (truePositive)/float(truePositive + falsePositive)
		recall = (truePositive)/float(truePositive + falseNegative)
		fMeasure = (truePositive + truePositive)/float(truePositive + truePositive + falseNegative + falsePositive)

		fold += 1
		print("Performance metrics for fold ", fold)
		print("accuracy=", accuracy)
		print("precision=", precision)
		print("recall=", recall)
		print("fMeasure=", fMeasure)
		print("===========================")

		total_accuracy += accuracy
		total_precision += precision
		total_recall += recall
		total_fMeasure += fMeasure

	print("Average accuracy= ", total_accuracy/folds)
	print("Average precision= ", total_precision/folds)
	print("Average recall= ", total_recall/folds)
	print("Average fMeasure= ", total_fMeasure/folds)


filePath = sys.argv[1]
random_forest(10, filePath, maxDepth=1000, minSize=0, numTrees=10, numFeatures = 0.3)
