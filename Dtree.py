import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
from collections import Counter



global current_Point
global data_fold
global dataType

np.set_printoptions(threshold=np.nan, suppress=True)

class Point:
    def __init__(self, continousVals, categoricalVals, groudTruth):
        self.contVals = np.array(continousVals)
        self.catVals  = np.array(categoricalVals)
        self.truth = -1
        self.groundTruth = groudTruth

    def getContVals(self):
        return self.contVals

    def getCatVals(self):
        return self.catVals

    def getGroundTruth(self):
        return self.groundTruth

    def getTruth(self):
        return self.truth

class Node:

    def __init__(self, value, col, contorCat):
        self.label = -1
        self.isCont = not contorCat
        self.value = value
        self.isLeaf = False
        self.col = col
        self.left = None
        self.right = None

    def getLeft(self):
        return self.left

    def getRight(self):
        return self.right




def detectDataTypes(data):

    dataType = []

    for col in data.columns.values:
        if data[col].dtypes=='object':
            dataType.append(1)
        else:
            dataType.append(0)

    return dataType


def getFrequentClass(training_set):

    truthlabellist = []

    for i in range(0, len(training_set)):
        truthlabellist.append(training_set[i].getGroundTruth())

    count = Counter(truthlabellist)
    return count.most_common()[0][0]


def get_gini(list):

    truthvalues = set(list)
    count = 0

    for values in truthvalues:
        count += (list.count(values))**2

    gini = (1 - ((float(count) / len(list)**2)))
    # print(gini)
    return gini

def getginiindex(training_set, col, currentVal, isCont):

    true_list = []
    false_list = []

    for row in range(0, len(training_set)):
        currentRecord = training_set[row]
        if(isCont):
            valarr = currentRecord.getContVals()
            if(valarr[col] >= currentVal):
                true_list.append(currentRecord.getGroundTruth())
            else:
                false_list.append(currentRecord.getGroundTruth())
        else:
            valarr = currentRecord.getCatVals()
            if(valarr[col] == currentVal):
                true_list.append(currentRecord.getGroundTruth())
            else:
                false_list.append(currentRecord.getGroundTruth())

    if(len(true_list) == 0 or len(false_list) == 0):
        return 100;

    total_len = float(len(true_list) + len(false_list))
    gini = get_gini(true_list)*(len(true_list)/total_len) + get_gini(false_list)*(len(false_list)/total_len)

    # print(gini)
    return gini


def getbestsplit(training_set):

    bestgini = 1.0
    bestval = -1
    bestcol = -1
    isCatSplit = False

    currentrecord = training_set[0]
    colSize = len(currentrecord.getContVals())

    #  Continous Values
    for col in range(0, colSize):
        visitedSet = set()
        for row in range(0, len(training_set)):
            currentrecord = training_set[row].getContVals()
            currentVal = currentrecord[col]
            if(currentVal in visitedSet):
                continue
            visitedSet.add(currentVal)
            ginicurrent = getginiindex(training_set, col, currentVal, True)
            # print ginicurrent

            if(ginicurrent < bestgini):
                bestgini = ginicurrent
                bestval = currentVal
                bestcol = col
                isCatSplit = False


    #  Categorical Values
    currentrecord = training_set[0]
    colSize = len(currentrecord.getCatVals())

    for col in range(0, colSize):
        visitedSet = set()
        for row in range(0, len(training_set)):
            currentrecord = training_set[row].getCatVals()
            currentVal = currentrecord[col]
            if(currentVal in visitedSet):
                continue
            visitedSet.add(currentVal)
            ginicurrent = getginiindex(training_set, col, currentVal, False)
            # print ginicurrent

            if(ginicurrent < bestgini):
                bestgini = ginicurrent
                bestval = currentVal
                bestcol = col
                isCatSplit = True

    if bestgini == 1.0:
        bestgini = 0

    # print "return value = ",bestgini, bestval, bestcol, isCatSplit
    return bestgini, bestval, bestcol, isCatSplit


def partition(training_set, value, col, isCatSplit):
    leftset = []
    rightset = []

    # print value, col, isCatSplit
    for row in range(0, len(training_set)):
        currentrecord = training_set[row]
        if(isCatSplit):
            currentvals = currentrecord.getCatVals()
            if(value == currentvals[col]):
                leftset.append(currentrecord)
            else:
                rightset.append(currentrecord)
        else:
            currentvals = currentrecord.getContVals()
            if(currentvals[col] >= value):
                leftset.append(currentrecord)
            else:
                rightset.append(currentrecord)

    # print("left: ", leftset)
    # print("right: ", rightset)
    # print("----------")
    return leftset, rightset


def constructDecisionTree(training_set, maxDepth, minSize, depth = 0):

    if depth >= maxDepth or len(training_set) <= minSize:
        root = Node(-1, -1, True)
        root.isLeaf = True
        root.value = getFrequentClass(training_set)
        return root

    gini, value, col , isCatSplit = getbestsplit(training_set)
    # print "outside = ",gini, value, col, isCatSplit

    root = Node(value, col, isCatSplit)

    if(gini == 0):
        root.isLeaf = True
        root.value = getFrequentClass(training_set)
        return root

    leftset, rightset = partition(training_set, value, col, isCatSplit)

    if (len(leftset)==0 or len(rightset)==0):
        root.isLeaf = True
        root.value = getFrequentClass(training_set)
        return root

    # print("contructing subtrees")
    root.left = constructDecisionTree(leftset,maxDepth,minSize, depth+1)
    root.right = constructDecisionTree(rightset,maxDepth,minSize, depth+1)

    return root


def predictclass(test_sample, rootnode):
    if rootnode.isLeaf:
        return rootnode.value

    if(rootnode.isCont):
        current_value = test_sample.getContVals()[rootnode.col]
        if(current_value >= rootnode.value):
            return predictclass(test_sample, rootnode.left)
        else:
            return predictclass(test_sample, rootnode.right)
    else:
        # print(test_sample.getCatVals())
        # print(rootnode.col)
        current_value = test_sample.getCatVals()[rootnode.col]
        if(current_value == rootnode.value):
            return predictclass(test_sample, rootnode.left)
        else:
            return predictclass(test_sample, rootnode.right)

def depth(root):
    if root.isLeaf:
        return 1

    # print("===================")
    # print(root.isCont)
    # print(root.col , ":" , root.value)
    # print("===================")

    return 1+max(depth(root.left), depth(root.right))

def decisiontree(folds, file, maxDepth, minSize):

    global data_fold, current_Point, dataType

    data_fold = folds

    data = pd.read_csv(file, delimiter = '\t', header=None)

    dataType = detectDataTypes(data.loc[[0]])
    dataType = np.array(dataType)

#     Generate point objects
    dataPoints = []
    col_size = len(dataType)

    for row in range(0, len(data)):
        # record = np.array(data.loc[row])
        # numericalData = record[dataType]
        # categoricalData = record[1-dataType]
        numericalData = []
        categoricalData = []

        for col in range(0, col_size-1):
            if(dataType[col] == 0):
                numericalData.append(data[col][row])
            else:
                categoricalData.append(data[col][row])

        truth = (data.loc[[row]][col_size-1])[row]

        if(truth == 1):
            newPoint = Point(numericalData, categoricalData, 1)
            dataPoints.append(newPoint)
        else:
            newPoint = Point(numericalData, categoricalData, 0)
            dataPoints.append(newPoint)


    kf = KFold(folds)
    kf.get_n_splits(dataPoints)

    dataPoints = np.array(dataPoints)

    for train_index, test_index in kf.split(dataPoints):

        training_set = dataPoints[train_index]
        test_set = dataPoints[test_index]
        rootnode = constructDecisionTree(training_set, maxDepth, minSize)

        # print("depth =", depth(rootnode))



        for test_sample in test_set:
            test_sample.truth = predictclass(test_sample, rootnode)




    positive = 0
    negative = 0

    for point in dataPoints:
        if(point.groundTruth == point.truth):
            positive+=1
        else:
            negative+=1

    print("In Dtree")
    print(float(positive)/(positive+negative))




decisiontree(10, 'Input/Project3_dataset2.txt', maxDepth=9999, minSize=0)