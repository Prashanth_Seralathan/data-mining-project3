import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import scale
from scipy.stats import zscore
from sklearn.preprocessing import normalize
import numpy as np
import heapq
import collections
from collections import Counter

global current_Point
global kNeighbours
global dataType

class Point:
    def __init__(self, dataPoints, classLabel):
        self.x = np.array(dataPoints)
        self.truth = -1
        self.groundTruth = classLabel

    def getPointValue(self):
        return self.x

    def getGroundTruth(self):
        return self.groundTruth

    def getTruth(self):
        return self.truth

def distance(point1, point2):
    global dataType

    continousDiffSum = 0

    for idx in range(len(point1)):
        continousDiffSum += np.square((point1[idx] - point2[idx]))

    dist = np.sqrt(continousDiffSum)
    return dist


def checkDistance(current_Point, data):
    heap = []

    for row in range(0, len(data)):
        heap.append(data[row])

    heap.sort(key = lambda point : distance(point.getPointValue(), current_Point.getPointValue()))

    #Calculating the true cluster
    truthFromNearestNeighbors = []
    for i in range(0, kNeighbours):
        currentElement = heap.pop(0)
        truthFromNearestNeighbors.append(currentElement.getGroundTruth())

    count = Counter(truthFromNearestNeighbors)


    current_Point.truth = count.most_common()[0][0]

    # if(count.most_common()[0][1] == (len(truthFromNearestNeighbors)/2)):
    #     current_Point.truth = 0

def minmaxnormalization(file):

    global kNeighbours, current_Point, dataType

    X_train=pd.read_csv(file, delimiter = '\t', header=None)

    le=LabelEncoder()

    dataType = []

    for col in X_train.columns.values:
        if X_train[col].dtypes=='object':
            dataType.append(False)
            data=X_train[col].append(X_train[col])
            le.fit(data.values)
            X_train[col]=le.transform(X_train[col])
        else:
            dataType.append(True)

    dataType[-1] = False #Final column

    data = X_train.values


    data = np.array(data)

    low =  0.0
    high = 1.0

    mins = np.min(data, axis = 0)
    maxs = np.max(data, axis = 0)
    rng = maxs - mins

    data = high - (((high - low) * (maxs-data)) / rng)

    return data, np.shape(data)[1]



def normalizeData(file):

    global kNeighbours, current_Point, dataType

    X_train=pd.read_csv(file, delimiter = '\t', header=None)

    le=LabelEncoder()

    dataType = []

    for col in X_train.columns.values:
        if X_train[col].dtypes=='object':
            dataType.append(False)
            data=X_train[col].append(X_train[col])
            le.fit(data.values)
            X_train[col]=le.transform(X_train[col])
        else:
            dataType.append(True)

    dataType[-1] = False #Final column

    data = X_train.values


    data = np.array(data)

    data_clone = data[:,np.array(dataType)]

    # data_clone_cont = normalize(data_clone, norm='l2')
    # data_clone_cont = scale(data_clone, axis=0, with_mean=True, with_std=True, copy=True)
    data_clone_cont = zscore(data_clone, axis=0)


    dataTypeclone = np.copy(dataType)
    dataTypeclone = np.logical_not(dataTypeclone)

    data_clone_catg = data[:,np.array(dataTypeclone)]

    formatted_data = np.append(data_clone_cont, data_clone_catg, axis=1)

    return formatted_data, np.shape(data_clone_cont)[1]

def read_data(file):

    data = np.genfromtxt(file,delimiter='\t', dtype=float)

    colSize = np.shape(data)[1]
    
    data =  np.delete(data, [colSize-1], axis=1)

    classLabels = open(file, "r").readlines()
    classLabels = [x.split("\t")[-1].split("\n")[0] for x in classLabels]
    classLabels = np.array(classLabels)

    return data, classLabels


def znorm(train_x, test_x):

    mean = np.mean(train_x, axis = 0)
    
    std = np.std(train_x, axis = 0)

    var = np.power(std, 2)
    train_x = (train_x - mean)/std

    test_x = (test_x - mean)/std

    return train_x, test_x



def Knn(neighbors, train_file, test_file):

    global kNeighbours, current_Point, dataType

    kNeighbours = neighbors

    # formatted_training_data, contValsLimit = normalizeData(train_file)
    # formatted_test_data, contValsLimit = normalizeData(test_file)

    # formatted_training_data, contValsLimit = minmaxnormalization(train_file)
    # formatted_test_data, contValsLimit = minmaxnormalization(test_file)

    # dataType = []

    # for idx in range(0, np.shape(formatted_training_data)[1]):
    #     if(idx < contValsLimit):
    #         dataType.append(0)
    #     else:
    #         dataType.append(1)


    train_x, train_y = read_data(train_file)
    test_x, test_y = read_data(test_file)

    formatted_training_data, formatted_test_data = znorm(train_x, test_x)

    trainingPoints = []

    for i in range(0, len(formatted_training_data)):
        trainingPoints.append(Point(formatted_training_data[i], train_y[i]))

    testingPoints = []
    for j in range(0, len(formatted_test_data)):
        testingPoints.append(Point(formatted_test_data[j], test_y[j]))

    truePositive = 0
    falseNegative = 0
    falsePositive = 0
    trueNegative = 0


    for j in range(0, len(testingPoints)):
        current_Point = testingPoints[j]
        checkDistance(current_Point, trainingPoints)

        # print(int(current_Point.getTruth()), int(current_Point.getGroundTruth()))
        if int(current_Point.getTruth()) == int(current_Point.getGroundTruth()):
            # print("same")
            if(int(current_Point.getTruth())==1):
                truePositive += 1
            else:
                trueNegative += 1
        else:
            if (int(current_Point.getTruth()) == 1):
                falsePositive += 1
            else:
                falseNegative += 1

    print(truePositive, trueNegative, falsePositive, falseNegative)

    accuracy = (truePositive + trueNegative) / float(truePositive + falseNegative + falsePositive + trueNegative)
    precision = (truePositive)/float(truePositive + falsePositive)
    recall = (truePositive)/float(truePositive + falseNegative)
    fMeasure = (truePositive + truePositive)/float(truePositive + truePositive + falseNegative + falsePositive)

    print("Performance metrics for Neighbours " , neighbors)
    print("accuracy=", accuracy)
    print("precision=", precision)
    print("recall=", recall)
    print("fMeasure=", fMeasure)
    print("===========================")



for i in range(1, 30):
    Knn(i, '../Input/project3_dataset3_train.txt', '../Input/project3_dataset3_test.txt')



