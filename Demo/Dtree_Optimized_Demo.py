import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import copy
from collections import Counter

from Decision_tree_Demo import *

global current_Point
global data_fold
global dataType

np.set_printoptions(threshold=np.nan, suppress=True)

def decisiontree(folds, file, maxDepth, minSize, numFeatures):

    global data_fold, current_Point, dataType

    data_fold = folds

    data = pd.read_csv(file, delimiter = '\t', header=None)

    dataType = detectDataTypes(data.loc[[0]])
    dataType = np.array(dataType)
    
    dataPoints = []
    col_size = len(dataType)

    for row in range(0, len(data)):
        numericalData = []
        categoricalData = []

        for col in range(0, col_size-1):
            if(dataType[col] == 0):
                numericalData.append(data[col][row])
            else:
                categoricalData.append(data[col][row])

        truth = (data.loc[[row]][col_size-1])[row]

        if(truth == 1):
            newPoint = Point(numericalData, categoricalData, 1)
            dataPoints.append(newPoint)
        else:
            newPoint = Point(numericalData, categoricalData, 0)
            dataPoints.append(newPoint)


    training_set = np.array(dataPoints)
    rootnode = constructDecisionTree(training_set, maxDepth, minSize, numFeatures, 0)

    h = getHeight(rootnode)
    rows = h
    cols = (2 ** h) - 1
    list = [["" for i in range(cols)] for j in range(rows)]

    populate(rootnode, list, 0, rows, 0, cols)
    for i in range(len(list)):
        each =list[i]
        str=""
        lenCount=6
        if(i!=0):
            j=0
            for value in each:
                count = lenCount
                if(len(value)==0):
                    for i in range(lenCount):
                        value = " " + value
                else:
                    j+=1
                    if(j%2==1):
                        value="  (YES)"
                    else:
                        value = "   (NO)"
                    count -= len(value)
                    for i in range(int(count / 2)):
                        value = " " + value
                    for i in range(count - int(count / 2)):
                        value += " "
                str += value
            print(str)
            str=""
        for value in each:
            count=lenCount
            count-=len(value)
            for i in range(int(count/2)):
                value=" "+value
            for i in range(count-int(count/2)):
                value+=" "
            str+=value
        print(str+"\n")

def populate(testNode, list, rows, rowe, cols, cole):
    if (rows == rowe or testNode == None):
        return
    list[rows][int((cols + cole) / 2)] = visualize(testNode)
    populate(testNode.left, list, rows + 1, rowe, cols, (cols + cole) / 2 - 1)
    populate(testNode.right, list, rows + 1, rowe, (cols + cole) / 2 + 1, cole)

def visualize(testNode):
    if (testNode.isLeaf == True):
        return "Class : " + str(testNode.value)
    if (testNode.isCont == True):
        return "is value >= " + str(testNode.value) + "?"
    else:
        return "is " + str(testNode.value) + "?"

def getHeight(testNode):
    if (testNode == None):
        return 0
    return 1 + max(getHeight(testNode.left), getHeight(testNode.right))

decisiontree(10, '../Input/project3_dataset4.txt', maxDepth=0, minSize=0, numFeatures=1)