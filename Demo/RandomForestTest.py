import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import copy
from collections import Counter
from random import randrange
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

global current_Point
global data_fold
global dataType

np.set_printoptions(threshold=np.nan, suppress=True)

def random_forest(folds, file, maxDepth, minSize, numTrees, numFeatures):
	global data_fold, current_Point, dataType
	data_fold = folds

	data = pd.read_csv(file, delimiter = '\t', header=None)


	col_size = 0
	for col in data.columns.values:
		col_size += 1
#     Generate point objects


	dataPoints = []
	dataLabels = []

	for row in range(0, len(data)):
		values= []
		for col in range(0, col_size-1):
			values.append(data[col][row])
		dataPoints.append(values)
		dataLabels.append(data[col_size-1][row])


	dataPoints = np.array(dataPoints)
	dataLabels = np.array(dataLabels)

	kf = KFold(data_fold)
	kf.get_n_splits(dataPoints)

	dataPoints = np.array(dataPoints)

	for train_index, test_index in kf.split(dataPoints):

		training_set = dataPoints[train_index]
		training_set_label = dataLabels[train_index]
		test_set = dataPoints[test_index]
		testing_set_label = dataLabels[test_index]

		clf = RandomForestClassifier(n_estimators=10, criterion="gini", max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features="auto", max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, bootstrap=True, oob_score=False, n_jobs=1, random_state=None, verbose=0, warm_start=False, class_weight=None)
		#	 print(np.shape(training_set), np.shape(training_set_label))
		trained_model = clf.fit(training_set, training_set_label)
		predictions = trained_model.predict(test_set)

		print("Test Accuracy :: ", accuracy_score(testing_set_label, trained_model.predict(test_set)))

random_forest(10, 'Input/project3_dataset1.txt', maxDepth=1000, minSize=0, numTrees=20, numFeatures = 0.3)