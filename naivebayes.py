import pandas as pd
from sklearn.model_selection import KFold
import numpy as np
import sys

np.set_printoptions(threshold=np.nan, suppress=True)

global current_Point
global data_fold
global dataType

class Point:
    def __init__(self, continousVals, categoricalVals, groudTruth):
        self.contVals = np.array(continousVals)
        self.catVals  = np.array(categoricalVals)
        self.truth = -1
        self.groundTruth = groudTruth

    def getContVals(self):
        return self.contVals

    def getCatVals(self):
        return self.catVals

    def getGroundTruth(self):
        return self.groundTruth

    def getTruth(self):
        return self.truth



# Generates the datatypes of the columns
def detectDataTypes(data):

    dataType = []

    for col in data.columns.values:
        if data[col].dtypes=='object':
            dataType.append(1)
        else:
            dataType.append(0)

    return dataType

def normpdf(x, mean, sd):
    x = np.array(x)
    mean = np.array(mean)
    sd = np.array(sd)


    var = np.power(sd, 2)
    pi = 3.1415926
    denom = np.power((2*pi*var), 0.5)
    power = np.power((x-mean), 2)/(2*var)
    num = np.exp(-power)
    return num/denom

def probability_continuous(test_sample, continuous_values):

    continuous_values = np.array(continuous_values)
    mean = np.mean(continuous_values, axis = 0)
    std = np.std(continuous_values, axis = 0)
    probablities = normpdf(test_sample, mean, std)
    product = np.prod(probablities)
    return product

def probability_categorical(test_sample_cat_values, categorical_values):

    probability_cat = []

    for col in range(0, len(test_sample_cat_values)):
        currentCatVal = test_sample_cat_values[col]
        probability_cat.append(0)
        for row in range(0, len(categorical_values)):
            if(currentCatVal == categorical_values[row][col]):
                probability_cat[col] += 1

    probability = 1

    for i in range(0, len(probability_cat)):
        probability *= (probability_cat[i] / (float)(len(categorical_values)))

    return probability

def naivebayes(folds, file):
    global data_fold, current_Point, dataType

    data_fold = folds

    data = pd.read_csv(file, delimiter = '\t', header=None)

    # print(data.loc[[0]])
    dataType = detectDataTypes(data.loc[[0]])
    dataType = np.array(dataType)

    dataPoints = []

    col_size = len(dataType)
    for row in range(0, len(data)):
        # record = np.array(data.loc[row])
        # numericalData = record[dataType]
        # categoricalData = record[1-dataType]
        numericalData = []
        categoricalData = []
        for col in range(0, col_size-1):
            if(dataType[col] == 0):
                numericalData.append(data[col][row])
            else:
                categoricalData.append(data[col][row])


        truth = (data.loc[[row]][col_size-1])[row]

        if(truth == 1):
            newPoint = Point(numericalData, categoricalData, 1)
            dataPoints.append(newPoint)
        else:
            newPoint = Point(numericalData, categoricalData, 0)
            dataPoints.append(newPoint)


    kf = KFold(n_splits=folds)
    kf.get_n_splits(dataPoints)

    dataPoints = np.array(dataPoints)

    total_accuracy = 0
    total_precision = 0
    total_recall = 0
    total_fMeasure = 0

    fold = 0
    for train_index, test_index in kf.split(dataPoints):
        # print(train_index, test_index)

        training_set = dataPoints[train_index]
        test_set = dataPoints[test_index]

        training_set_size = len(training_set)

        continuous_values = []
        categorical_values = []

        continuous_values_positive = []
        categorical_values_positive = []

        continuous_values_negative = []
        categorical_values_negative = []

        for point in training_set:
            continuous_values.append(point.getContVals())
            categorical_values.append(point.getCatVals())
            
            if point.getGroundTruth() == 0:
                continuous_values_positive.append(point.getContVals())
                categorical_values_positive.append(point.getCatVals())
            else:
                continuous_values_negative.append(point.getContVals())
                categorical_values_negative.append(point.getCatVals())

        # P(H0), P(H1)
        probability_positive = len(continuous_values_positive)/float(training_set_size)
        probability_negative = len(continuous_values_negative)/float(training_set_size)

        truePositive = 0
        falseNegative = 0

        falsePositive = 0
        trueNegative = 0

        for test_sample in test_set:

            # P(X)
            probability_cont = probability_continuous(test_sample.getContVals(), continuous_values)
            probability_cat = probability_categorical(test_sample.getCatVals(), categorical_values)

            # P(X/H0)
            probability_continuous_pos = probability_continuous(test_sample.getContVals(), continuous_values_positive)
            probability_categorical_pos = probability_categorical(test_sample.getCatVals(), categorical_values_positive)

            # P(X/H1)
            probability_continuous_neg = probability_continuous(test_sample.getContVals(), continuous_values_negative)
            probability_categorical_neg = probability_categorical(test_sample.getCatVals(), categorical_values_negative)

            # P(H0/X)
            prob_pos_given_x = probability_positive*probability_continuous_pos*probability_categorical_pos/(probability_cont*probability_cat)

            # P(H0/X)
            prob_neg_given_x = probability_negative*probability_continuous_neg*probability_categorical_neg/(probability_cont*probability_cat)

            # print(prob_pos_given_x, prob_neg_given_x)

            if(prob_pos_given_x > prob_neg_given_x):
                test_sample.truth = 0
            else:
                test_sample.truth = 1

        

            if test_sample.getTruth() == test_sample.getGroundTruth():
                if test_sample.getTruth() == 1:
                    truePositive += 1
                else:
                    trueNegative += 1
            else:
                if test_sample.getTruth() == 1:
                    falsePositive += 1
                else:
                    falseNegative += 1

        accuracy = (truePositive + trueNegative) / float(truePositive + falseNegative + falsePositive + trueNegative)
        precision = (truePositive)/float(truePositive + falsePositive)
        recall = (truePositive)/float(truePositive + falseNegative)
        fMeasure = (truePositive + truePositive)/float(truePositive + truePositive + falseNegative + falsePositive)
        
        fold += 1
        print("Performance metrics for fold ", fold)
        print("accuracy=", accuracy)
        print("precision=", precision)
        print("recall=", recall)
        print("fMeasure=", fMeasure)
        print("===========================")

        total_accuracy += accuracy
        total_precision += precision
        total_recall += recall
        total_fMeasure += fMeasure

    print("Average accuracy= ", total_accuracy/folds)
    print("Average precision= ", total_precision/folds)
    print("Average recall= ", total_recall/folds)
    print("Average fMeasure= ", total_fMeasure/folds)


filePath = sys.argv[1]
naivebayes(10, filePath)